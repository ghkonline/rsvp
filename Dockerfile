FROM nginx
COPY ./dist /usr/share/nginx/html/dist
COPY index.html /usr/share/nginx/html/index.html
COPY style.css /usr/share/nginx/html/style.css
COPY config.js /usr/share/nginx/html/config.js
COPY Invitation.jpg /usr/share/nginx/html/Invitation.jpg
COPY BackGround.jpeg /usr/share/nginx/html/BackGround.jpeg
COPY favicon.png /usr/share/nginx/html/favicon.png
EXPOSE 80