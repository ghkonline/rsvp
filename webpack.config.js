var path = require('path');

module.exports = [{
    entry: {
        app: './src/index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: './app.js'
    },
    devServer: {
        contentBase: path.join(__dirname, './'),
        compress: true,
        port: 9000,
        writeToDisk: true
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    }
}];