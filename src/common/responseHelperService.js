import { NotificationComponent } from '../common/notification'

export class ResponseHelperService {
    async processResponse(that, source, response) {
        if (response.status == 200) return true;
        if (response.status == 500) {
            var reader = response.body.getReader();
            var data = await reader.read();
            var decoder = new TextDecoder();
            var chunk = decoder.decode(data.value || new Uint8Array, { stream: !data.done });
            that._showNotification(`${source}: ${chunk}`, NotificationComponent.error);
        }
    }
}