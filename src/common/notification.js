import { LitElement, html, css } from 'lit-element/lit-element';

export class NotificationComponent extends LitElement {
    static get is() {
        return `notification-com`;
    }

    static get properties() {
        return {
            timeSpan: Number,
            message: String,
            messageType: String,
            display: Boolean
        }
    }

    constructor() {
        super();
        this.timeSpan = 0;
        this.message = ``;
        this.messageType = ``;
    }

    static get success() {
        return `success`;
    }
    static get error() {
        return `error`;
    }
    static get info() {
        return `info`;
    }

    static get styles() {
        return css`
        #notification {
            position:absolute;
            top:0;
            right:0;
            margin:0.5rem;
            padding:0.2rem;
            z-index:1000;
            height:3rem;
            width:25rem;
            border-radius:3px 3px;
            color:#000000;
            font-weight:bold;
        }
        .message{
            padding-left:1rem;
            margin:0.2rem;
            overflow-wrap: break-word;
        }
        .success{
            background: #61ffd2;
        }
        .error{
            background: #fa5b11
        }
        .info{
            background: #72cbe8
        }
        .warning{
            background: #f0ea7f
        }
        `;
    }

    render() {
        let { message, display, messageType } = this;
        return display ? html`
            <div id="notification" class=${messageType}>
                <div class="message">${message}</div>
            </div>
        `
            : html``;
    }
}

customElements.define(NotificationComponent.is, NotificationComponent);