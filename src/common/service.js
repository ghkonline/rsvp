export class apiservice {
    constructor() { }

    async get(url) {
        return fetch(`${window.service.url}${url}`).then((response) => {
            return response;
        });
    }

    async post(url, data) {
        var options = {
            method: `POST`,
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data)
        }
        return fetch(`${window.service.url}${url}`, options).then((response) => {
            return response;
        });
    }
}