import { ResponseHelperService } from './common/responseHelperService';
import { NotificationComponent } from './common/notification';
import { apiservice } from './common/service';

export class RsvpService {
    get responseHelper() {
        return new ResponseHelperService();
    }

    get Service() {
        var service = new apiservice();
        return service;
    }

    async GetRsvp(that, token) {
        return await this.Service.get(`Rsvp/${token}`)
            .then((response) => {
                if (!this.responseHelper.processResponse(that, `Getting Rsvp details`, response)) return false;
                if(response.status == 200){
                    return response.json();
                }
            })
            .catch((error)=>{
                console.error(error);
                return false;
            })
    }

    async SubmitRsvp(that, rsvpObject) {
        return await this.Service.post(`Rsvp`, rsvpObject)
            .then((response) => {
                if (!this.responseHelper.processResponse(that, `Submitting Rsvp`, response)) return { message: "Something went wrong", status: false };
                if (response.status == 200) {
                    return response.json();
                }
            })
            .catch((error) => {
                console.error(error);
                return false
            });
    }
}