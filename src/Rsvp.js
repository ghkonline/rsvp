import { LitElement, css, html } from 'lit-element';
import { render } from 'lit-html';
import '@vaadin/vaadin-dialog';
import '@vaadin/vaadin-text-field/vaadin-text-area';
import '@vaadin/vaadin-text-field/vaadin-number-field';
import '@vaadin/vaadin-form-layout/vaadin-form-layout';
import '@vaadin/vaadin-form-layout/vaadin-form-item';
import '@vaadin/vaadin-button';
import { RsvpService } from './RsvpService';
import { NotificationComponent } from './common/notification';

export class RsvpComponent extends LitElement {
    static get is() {
        return `rsvp-com`;
    }

    static get styles() {
        return css `
            .button {
                background-color: #01afae;
                border: 1px solid #c0c0c0;
                border-radius: 5px 5px 5px;
                padding: 2rem;
                font-size:150%;
                color:#ffffff;
                cursor: pointer;
                width:15rem;
            }
            .paddingLeft5rem {
                padding-left:5rem;
            }
            .container {
                display: flex;
                justify-content:center;
            }
            .image {
                position: relative;
                display: block;
                margin-top:1rem;
                margin-left: auto;
                margin-right: auto;
                width: 70%;
            }
            #greetingText {
                color:#ffffff;
                font-size:2em;
                text-align:center;
                font-style:italic
            }
        `;
    }

    render() {
            let { _loadApp, _loadingMessage, rsvpInfo } = this;
            return _loadApp ? html `
            <div id="greetingText">Hello, ${rsvpInfo ? rsvpInfo.guestName : ``}<div>
            <div class="container">
                <button
                    class="button"
                    @click=${this._presenceRSVP}
                >I'm in</button>
                <span class="paddingLeft5rem"></span>
                <button
                    class="button"
                    @click=${this._absenceRSVP}
                >I Can't</button>
                <vaadin-dialog
                    id="present"
                    draggable
                    resize
                    .renderer=${this._presentDialogRenderProp}>
                </vaadin-dialog>
                <vaadin-dialog
                    id="absent"
                    draggable
                    resize
                    .renderer=${this._absentDialogRenderProp}>
                </vaadin-dialog>
                <vaadin-dialog
                    id="confirmation"
                    draggable
                    resize
                    .renderer=${this._confirmationDialogRenderProp}>
                </vaadin-dialog>
            </div>
            <img src="Invitation.jpg" class="image" alt="image not loaded" />
        ` : html `<div class="container"><h1>${_loadingMessage}</h1></div>`;
    }

    static get properties() {
        return {
            token: String,
            rsvpInfo: Object,
            _rsvp: Object,
            _service: Object,
            _message: String,
            _smiley: String,
            _loadApp: Boolean,
            _loadingMessage: String
        };
    }

    constructor() {
        super();
        this._loadApp = false;
        this._loadingMessage = "Loading...";
        this._presentDialogRenderProp = this._presentDialogRenderer.bind(this);
        this._absentDialogRenderProp = this._absentDialogRenderer.bind(this);
        this._confirmationDialogRenderProp = this._confirmationDialogRender.bind(this);
        this._service = new RsvpService();
    }

    updated(changedProps) {
        if (changedProps.has(`token`) && this.token != undefined && this.token != null) {
            if (this._rsvp) this._rsvp.token = this.token;
            this._getRsvpInfo(this.token);
        }
    }

    get presentDialog() {
        return this.shadowRoot.querySelector(`#present`);
    }

    get absentDialog() {
        return this.shadowRoot.querySelector(`#absent`);
    }

    get confirmationDialog() {
        return this.shadowRoot.querySelector(`#confirmation`);
    }

    _presentDialogRenderer(root, dialog) {
        render(html `
                <div style="min-width:30rem;">
                    <vaadin-number-field 
                        id="guestCount"
                        label="Number of guests"
                        style="width:100%;"
                        @input=${(e) => this._guestCountChange(e)}
                        ></vaadin-number-field>
                    </br>
                    <vaadin-text-area 
                        id="message"
                        label="Message"
                        value="Congratulations!"
                        width="20rem"
                        style="width:100%;min-height:8rem;"
                        @input=${(e) => this._messageChange(e)}
                        ></vaadin-text-area>
                    </br>
                    <vaadin-button @click=${(e) => this._submitPresence(e)}>Submit</vaadin-button>
                </div>
            `, root);
    }

    _absentDialogRenderer(root, dialog) {
        render(html `
                <div style="min-width:30rem;">
                    <vaadin-text-area 
                        id="message"
                        label="Message"
                        value="Sorry..."
                        width="20rem"
                        style="width:100%;min-height:8rem;"
                        @input=${(e) => this._messageChange(e)}
                        ></vaadin-text-area>
                    </br>
                    <vaadin-button @click=${(e) => this._submitAbsence(e)}>Submit</vaadin-button>
                </div>
            `, root);
    }

    _confirmationDialogRender(root, dialog) {
        render(html `
            <div style="padding-left:1rem;padding-right:1rem;display:flex;justify-content:center;">
                <h3>${this._message}</h3>
            </div>
            <div style="padding:0px;display:flex;justify-content:center;font-size:100px;margin-top:-3rem;">
                ${this._smiley}
            </div>
        `, root);
    }

    _welcomeConfirmation() {
        this._message = html `Thank you, see you soon.`;
        this._smiley = html `&#128512;`;
    }

    _nextTimeConfirmation() {
        this._message = html `We will miss you at the event. Will see you next time.`;
        this._smiley = html `&#128577;`;
    }

    _guestCountChange(e) {
        var value = parseInt(e.target.value);
        this._rsvp.guestCount = value == null || value == undefined ? 0 : value;
    }

    _messageChange(e) {
        this._rsvp.message = e.target.value;
    }

    _presenceRSVP() {
        this._rsvp = { token: this.token == undefined ? "" : this.token, present: true, guestCount: 0, message: "Congratulations!" }
        this._rsvp.present = true;
        this.presentDialog.opened = true;
    }

    _absenceRSVP() {
        this._rsvp = { token: this.token ? this.token : "", present: false, guestCount: 0, message: "Sorry!" }
        this.absentDialog.opened = true;
    }

    async _getRsvpInfo(token) {
        this.rsvpInfo = await this._service.GetRsvp(this, token);
        if (this.rsvpInfo) this._loadApp = true;
        else {
            this._loadApp = false;
            this._loadingMessage = "Hmm... the url seems incorrect.";
        }
    }

    async _submitPresence() {
        var response = await this._service.SubmitRsvp(this, this._rsvp);
        if (response) this._showNotification(response.message, response.status ? NotificationComponent.info : NotificationComponent.error);
        this.presentDialog.opened = false;
        this._welcomeConfirmation();
        this.confirmationDialog.opened = true;
    }

    async _submitAbsence() {
        var response = await this._service.SubmitRsvp(this, this._rsvp);
        if (response) this._showNotification(response.message, response.status ? NotificationComponent.info : NotificationComponent.error);
        this.absentDialog.opened = false;
        this._nextTimeConfirmation();
        this.confirmationDialog.opened = true;
    }

    _showNotification(message, type) {
        var event = new CustomEvent(`showNotification`, { detail: { message: message, type: type } });
        this.dispatchEvent(event);
    }
}

customElements.define(RsvpComponent.is, RsvpComponent);